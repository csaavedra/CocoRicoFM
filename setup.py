# -*- coding: utf-8 -*-
# This file is part of CocoRicoFM.
#
# CocoRicoFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CocoRicoFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CocoRicoFM.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages
from cocoricofm import version

data_files = []

readme = open('README.rst').read()

setup(name="cocoricofm",
      version=version,
      description="A little radio player",
      long_description=readme,
      author="Philippe Normand",
      author_email='phil@base-art.net',
      license="GPL3",
      packages=find_packages(),
      include_package_data=True,
      package_data={
          'cocoricofm': ['templates/*.html', 'static/*.js', 'static/*.png'],
      },
      data_files=data_files,
      url="http://base-art.net",
      download_url='http://base-art.net/static/cocoricofm-%s.tar.gz' % version,
      keywords=['radio', 'multimedia', 'gstreamer', 'recording', 'gtk+',
                'libre.fm', 'last.fm'],
      classifiers=['Development Status :: 5 - Production/Stable',
                   'Environment :: Console',
                   'Environment :: MacOS X', 'Environment :: Web Environment',
                   'Environment :: X11 Applications :: GTK',
                   'Framework :: AsyncIO',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python :: 3.6',
                   'Topic :: Multimedia :: Sound/Audio :: Players',
                   'Intended Audience :: End Users/Desktop',
                   'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
                   ],
      entry_points="""\
      [console_scripts]
      cocoricofm = cocoricofm.main:main
      """,
)
