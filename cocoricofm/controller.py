# -*- coding: utf-8 -*-
# This file is part of CocoRicoFM.
#
# CocoRicoFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CocoRicoFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CocoRicoFM.  If not, see <http://www.gnu.org/licenses/>.

import configparser
import time
from gi.repository import GLib, GObject
import asyncio
import gbulb

from . import player, radios, pylast, lirc_input, denon, keyboard_input

try:
    from . import linux_notify
except ImportError:
    deskop_notify = None

try:
    from . import growl_notify
except ImportError:
    growl_notify = None

try:
    from . import osx_notify
except ImportError:
    osx_notify = None

try:
    from . import web_remote
except ImportError:
    print("web_remote disabled. Jinja2 and/or aiohttp_jinja2 missing?")
    web_remote = None

try:
    from . import ui
except:
    print("GTK+ UI disabled. PyGTK missing?")
    ui = None

class Controller(GObject.GObject):
    __gsignals__ = { 'station-changed': (GObject.SignalFlags.RUN_FIRST, GObject.TYPE_NONE, ()),
                     'next-refresh-in': (GObject.SignalFlags.RUN_FIRST, GObject.TYPE_NONE, (GObject.TYPE_FLOAT,)),
    }

    def __init__(self, options, config):
        super(Controller, self).__init__()
        gbulb.install(gtk = ui and options.gui)
        self.loop = gbulb.get_event_loop()
        self.interval = options.interval
        self.recording = options.record
        self.output_path = options.output
        self.disable_scrobble = options.noscrobble
        self.headless = options.headless
        self.ui_enabled = options.gui
        self.http_port = options.port
        self.config = config
        self.lastfm = None

        self.player = player.Player(self)
        self.player.connect("suspended", self._player_suspended)
        self.player.connect("resumed", self._player_resumed)

        self.station_name = options.station

        self.suspended = False

        self.current_status = None
        if not self.headless:
            if osx_notify:
                self.notification = osx_notify.Notification("CocoRicoFM")
            elif linux_notify:
                self.notification = linux_notify.Notification("CocoRicoFM")
            else:
                self.notification = None
        elif growl_notify:
            try:
                growl_credentials = [dict(self.config.items(s)) for s in self.config.sections() if s.startswith("growl")]
                self.notification = growl_notify.Notification("CocoRicoFM", growl_credentials)
            except Exception as exc:
                self.notification = None
        else:
            self.notification = None

        self.connect("station-changed", self._station_changed)

        if ui and self.ui_enabled:
            ui.init()
            self.ui = ui.GUI(self, self.player, self.main_quit)
        else:
            self.ui = None

        self.keyboard_input = keyboard_input.KeyboardInput(self)
        self.lirc_input = lirc_input.LircInput(config, self)

        try:
            denon_address = self.config.get("denon", "address")
        except configparser.NoSectionError:
            self.denon_remote = None
        else:
            self.denon_remote = denon.DenonRemote(denon_address)

        if web_remote and self.http_port > 0:
            self.web_remote = web_remote.WebRemote(self)
        else:
            self.web_remote = None

    def _station_changed(self, *args):
        asyncio.ensure_future(self.refresh())

    def tune(self, station_index):
        stations = list(radios.STATIONS.keys())
        stations.sort()
        if station_index in range(len(stations)):
            self.tune_station_with_name(stations[station_index])

    def tune_station_with_name(self, name):
        self.station_name = name
        self.emit("station-changed")

    def __setattr__(self, attr, value):
        super().__setattr__(attr, value)

        if attr == 'station_name':
            self.station = radios.STATIONS[self.station_name]()

            async def tune_url():
                live_url = await self.station.live_url()
                self.player.set_url(live_url)

            asyncio.ensure_future(tune_url())

    def _player_suspended(self, player):
        self.suspended = True
        if not self.current_status:
            return
        if not self.notification:
            return
        self.notification.clear_actions()
        self.notification.add_action("resume", "Resume playback", self._resume_playback_cb)
        self.notification.icon_name = "media-playback-stop-symbolic"
        asyncio.ensure_future(self.notification.show())

    def _player_resumed(self, player):
        self.suspended = False
        if not self.current_status:
            return
        if not self.notification:
            return
        self.notification.clear_actions()
        self.notification.add_action("suspend", "Suspend playback", self._suspend_playback_cb)
        self.notification.icon_name = "media-playback-start-symbolic"
        asyncio.ensure_future(self.notification.show())

    def _resume_playback_cb(self, notification, action):
        self.player.start()

    def _suspend_playback_cb(self, notification, action):
        self.player.stop()

    async def login(self):
        if self.disable_scrobble:
            return
        if not self.interval:
            return

        self.lastfm = self.librefm = None

        try:
            lastfm_username = self.config.get("scrobbler-lastfm", "user")
            lastfm_pw_hash = self.config.get("scrobbler-lastfm", "password_hash")
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            lastfm_username = None
            lastfm_pw_hash = None
        if lastfm_username and lastfm_pw_hash:
            self.lastfm = pylast.LastFMNetwork(api_key="623bbd684658a8eaaa4066037d3c1531",
                                               api_secret="547e71d1582dfb73f6857444992fa629",
                                               username=lastfm_username,
                                               password_hash=lastfm_pw_hash)
            await self.lastfm.authenticate()
            # TODO: local scrobble cache support
        try:
            librefm_username = self.config.get("scrobbler-librefm", "user")
            librefm_pw_hash = self.config.get("scrobbler-librefm", "password_hash")
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            librefm_username = None
            librefm_pw_hash = None
        if librefm_username and librefm_pw_hash:
            print("LibreFM support is temporarily disabled.")
            #self.librefm = pylast.get_librefm_network(username=librefm_username,
            #                                          password_hash=librefm_pw_hash)

    async def _retrieve_song_infos(self, current_status):
        artist_name = current_status.artist
        album_title = current_status.album
        track_name = current_status.title
        artist_nice_name = artist_name
        track_nice_name = track_name
        duration = 0
        mbid = ""
        if self.lastfm and artist_name and track_name:
            search_results = self.lastfm.search_for_track(artist_name, track_name)
            page = await self._execute_with_pylast(getattr(search_results, "get_next_page"))
            if page and len(page) > 0:
                track = page[0]
                if not album_title:
                    album = await track.get_album()
                    if album:
                        album_title = album.title
                duration = await self._execute_with_pylast(track.get_duration)
                if duration:
                    duration = int(duration / 1000.)
                else:
                    duration = 0
                mbid = await self._execute_with_pylast(track.get_mbid, __default="")
                artist_nice_name = track.artist.name
                track_nice_name = track.title

        return (artist_nice_name, album_title, track_nice_name, duration, mbid)

    async def _execute_with_pylast(self, function, *args, **kwargs):
        if '__default' in kwargs.keys():
            default_result = kwargs['__default']
            del kwargs['__default']
        else:
            default_result = None

        try:
            result = await function(*args, **kwargs)
        except Exception as exc:
            # Something went wrong, try 2 more times and die.
            attempts = 2
            while attempts > 0:
                try:
                    result = await function(*args, **kwargs)
                except Exception as exc:
                    attempts -= 1
                    continue
                else:
                    break
            if not attempts:
                print("Call to %r failed..." % function)
                result = default_result

        return result

    async def scrobble_update_now_playing(self, current):
        if self.disable_scrobble:
            return
        if not current:
            return

        artist_nice_name, album_title, track_nice_name, duration, mbid = await self._retrieve_song_infos(current)
        if self.lastfm:
            await self._execute_with_pylast(getattr(self.lastfm, "update_now_playing"), artist_nice_name, track_nice_name,
                                            album=album_title, duration=duration, mbid=mbid)

    async def scrobble_song(self, current_status):
        if self.disable_scrobble:
            return
        if not current_status:
            return
        artist_nice_name, album_title, track_nice_name, duration, mbid = await self._retrieve_song_infos(current_status)
        time_started = current_status.time_started
        if '' not in (artist_nice_name, track_nice_name):
            args = (artist_nice_name, track_nice_name, time_started)
            kwargs = dict(album=album_title)
            if mbid:
                kwargs['mbid'] = mbid
            if duration:
                kwargs['duration'] = duration
            for network in (self.lastfm, self.librefm):
                if not network:
                    continue
                await self._execute_with_pylast(getattr(network, "scrobble"), *args, **kwargs)

    def stop(self):
        if self.player:
            self.player.stop()
        self.loop.stop()

    def status(self, song_infos):
        artist = song_infos.artist
        album = song_infos.album
        title = song_infos.title
        status = "♫ %s - %s ♫" % (artist, title)
        if self.ui:
            asyncio.ensure_future(self.ui.update(song_infos))
        if self.notification:
            self.notification.update(self.station_name, status)
            if not self.notification.actions:
                self.notification.add_action("suspend", "Suspend playback", self._suspend_playback_cb)
            self.notification.icon_name = "media-playback-start-symbolic"
            asyncio.ensure_future(self.notification.show())

        GLib.setenv("PA_PROP_MEDIA_ARTIST", artist, True)
        GLib.setenv("PA_PROP_MEDIA_TITLE", title, True)
        return "%s: %s" % (self.station_name, status)

    async def refresh(self):
        delta = 0.0
        current = await self.station.now_playing()
        if "" not in (current.artist, current.title) and (not self.current_status or \
                                                          (current != self.current_status)):
            if self.station.advising_cache_time:
                next_update_ts = self.station.next_update_timestamp()
                if next_update_ts:
                    delta = next_update_ts - time.time()
                    if delta <= 0:
                        return 1.0
                else:
                    return 1.0

            if self.current_status and (self.current_status != current):
                self.player.song_changed(self.current_status)

            message = self.status(current)
            print(message)
            if self.current_status:
                await self.scrobble_song(self.current_status)
            await self.scrobble_update_now_playing(current)
            self.keyboard_input.grab_keys()
        elif self.station.advising_cache_time:
            delta = 1.0

        if not delta:
            delta = self.interval

        self.current_status = current
        return delta

    async def main(self):
        await self.login()

        while True:
            if self.suspended:
                while self.suspended:
                    await asyncio.sleep(1)

            delta = await self.refresh()
            self.emit('next-refresh-in', delta)
            try:
                await asyncio.sleep(delta)
            except asyncio.CancelledError:
                self.current_status = None
                if self.notification:
                    await self.notification.close()
                if self.lastfm:
                    await pylast.close_network_session()
                await radios.close_network_session()
                break

        self.stop()

    def main_quit(self):
        self.main_task.cancel()

    def run(self):
        self.main_task = asyncio.ensure_future(self.main())

        if self.web_remote:
            asyncio.ensure_future(self.web_remote.start())

        if self.ui:
            self.ui.start()

        asyncio.ensure_future(self.lirc_input.start())

        try:
            self.loop.run_until_complete(self.main_task)
        except KeyboardInterrupt:
            self.main_quit()
            try:
                self.loop.run_until_complete(self.main_task)
            except KeyboardInterrupt:
                pass
            self.main_task.exception()
        finally:
            self.loop.close()

        return 0
